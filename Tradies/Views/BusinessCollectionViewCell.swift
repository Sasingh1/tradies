//
//  BusinessCollectionViewCell.swift
//  Tradies
//
//  Created by Sartaj Singh on 5/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import UIKit

class BusinessCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var businessImageView: UIImageView!
}
