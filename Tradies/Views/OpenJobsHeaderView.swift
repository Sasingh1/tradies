//
//  OpenJobsHeaderView.swift
//  Tradies
//
//  Created by Sartaj Singh on 5/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import UIKit

class OpenJobsHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var dotsImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var postedDateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var connectedLabel: UILabel!
}
