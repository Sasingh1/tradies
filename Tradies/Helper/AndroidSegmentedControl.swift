//
//  AndroidSegmentedControl.swift
//  Tradies
//
//  Created by Sartaj Singh on 4/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import UIKit

open class AndroidSegmentedControl: UISegmentedControl {

    public override init(items: [Any]?) {
        super.init(items: items)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    private func setup() {
        let bundle = Bundle(for: type(of: self))
        let selectedImage = UIImage(named: "drawing-android-tab-selected", in:bundle, compatibleWith: nil)
        setBackgroundImage(selectedImage, for: .selected, barMetrics: .default)
        let notSelectedImage = UIImage(named: "drawing-android-tab-not-selected", in: bundle, compatibleWith: nil)
        setBackgroundImage(notSelectedImage, for: .normal, barMetrics: .default)
        setTitleTextAttributes([NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline),
                                NSAttributedString.Key.foregroundColor: UIColor(red: 87.0/255.0, green: 88.0/255.0, blue: 89.0/255.0, alpha: 1.0)],
                               for: .normal)
        setTitleTextAttributes([NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline), NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
    }
}
