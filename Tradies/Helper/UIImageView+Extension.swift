//
//  UIImageView+Extension.swift
//  Tradies
//
//  Created by Sartaj Singh on 5/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import UIKit

func downloadImage(from url: URL, completion: ((_ image: UIImage?) -> Void)? = nil) {
    URLSession.shared.downloadTask(with: url, completionHandler: { imageUrl, _, _ in
        if let image = (imageUrl.flatMap { try? Data(contentsOf: $0, options: []) }.flatMap(UIImage.init(data:))) {
            completion?(image)
        } else {
            completion?(nil)
        }
    }).resume()
}

extension UIImageView {
    func setImage(for url: URL?, placeholder: UIImage? = nil) {
        guard let url = url else {
            image = placeholder
            return
        }
        image = placeholder
        downloadImage(from: url) { image in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.backgroundColor = .clear
                self.image = image
            }
        }
    }
}
