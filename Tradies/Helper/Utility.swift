//
//  Utility.swift
//  Tradies
//
//  Created by Sartaj Singh on 6/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

func stringWithShortDateFormatter(date: String) -> String {
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd"
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd MMM yyyy"
    let date: NSDate? = dateFormatterGet.date(from: "2016-02-29 12:24:26") as NSDate?
    return dateFormatterPrint.string(from: date! as Date)
}
