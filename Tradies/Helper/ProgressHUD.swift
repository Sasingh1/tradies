//
//  ProgressHUD.swift
//  Tradies
//
//  Created by Sartaj Singh on 2/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation
import SVProgressHUD

class ProgressHUD {
    
    func ShowSVProgressHUD_Black() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        SVProgressHUD.show()
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setBackgroundColor( UIColor.black.withAlphaComponent(0.4))
        SVProgressHUD.setForegroundColor( UIColor.white)
        SVProgressHUD.setRingThickness( 1.0)
    }
    
    func DismissSVProgressHUD() {
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }
}
