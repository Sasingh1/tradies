//
//  JobsListHandler.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

class JobsListHandler {
    
    private let webService: WebService = WebService()
    
    func fetchJobsInfo(completion: @escaping ((Result<[JobsInformation], ErrorResult>)) -> Void ) {
        let jobsURL = APIManager.jobsURL
        let jobsResource = Resource<JobsMapResult>(url: jobsURL) { data in
            let jobsInformationModel = try? JSONDecoder().decode(JobsMapResult.self, from: data)
            return jobsInformationModel
        }
        self.webService.load(resource: jobsResource) { response in
            if
                let result = response,
                let jobsInfo = result.jobs
            {
                completion(.success(jobsInfo))
            } else {
                completion(.failure(.parser(string: "Error while parsing json data")))
            }
        }
    }
}
