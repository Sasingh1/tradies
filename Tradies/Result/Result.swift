//
//  Result.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

enum Result<T, E:Error> {
    case success(T)
    case failure(E)
}

public enum NetworkResult<T> {
    case success(T)
    case failure(Error)
}
