//
//  Dynamic.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

class Dynamic<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    func bindAndFire(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ v:T) {
        value = v
    }
}
