//
//  JobsListViewModel.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

class JobsListViewModel {
    
    let jobsList: Dynamic<[JobsInformation]>
    let isFinished: Dynamic<Bool>
    var onErrorHandling: ((ErrorResult?) -> Void)?
    
    let jobsListHandler = JobsListHandler()    
    
    init() {
        self.jobsList = Dynamic([])
        self.isFinished = Dynamic(false)
        self.fetchJobsInfo()
    }
    
    private func fetchJobsInfo() {
        self.jobsListHandler.fetchJobsInfo { [weak self] result in
            DispatchQueue.main.async {
                self?.isFinished.value = true
                switch result {
                case .success(let list) :
                    self?.jobsList.value = list
                    print("jobs list- \(list)")
                    break
                case .failure(let error) :
                    print("Parser error \(error)")
                    self?.onErrorHandling?(error)
                    break
                }
            }
        }
    }
    
    func stringWithShortDateFormatter(date: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy"
        let date: NSDate? = dateFormatterGet.date(from: date) as NSDate?
        return dateFormatterPrint.string(from: date! as Date)
    }
}
