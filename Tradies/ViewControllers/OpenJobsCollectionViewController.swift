//
//  OpenJobsCollectionViewController.swift
//  Tradies
//
//  Created by Sartaj Singh on 4/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import UIKit
import SVProgressHUD

class OpenJobsCollectionViewController: UICollectionViewController {
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    
    var viewModel =  JobsListViewModel()
    var arrayJobs = [JobsInformation]()
    var progressHUD: ProgressHUD { return ProgressHUD() }
    static var menuIdentifier = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewModel()
    }
    
    func setupViewModel() {
        self.viewModel.jobsList.bindAndFire { [weak self] list in
            DispatchQueue.main.async {
                self?.arrayJobs = list
                self?.collectionView.reloadData()
            }
        }
    }            
}
extension OpenJobsCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrayJobs.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let connectedBusinesses = arrayJobs[section].connectedBusinesses else { return 0 }
        return connectedBusinesses.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "openJobsIdentifier", for: indexPath) as! BusinessCollectionViewCell
        cell.backgroundColor = .black
        let businessPhoto = arrayJobs[indexPath.section].connectedBusinesses![indexPath.item].thumbnail
        cell.backgroundColor = .white
        let imageUrl = URL(string: businessPhoto!)
        cell.businessImageView.setImage(for: imageUrl)
        return cell
    }
}


// MARK: UICollectionViewDelegateFlowLayout
extension OpenJobsCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(" didSelectItemAt indexPath")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow: CGFloat  = 4.0
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "OpenJobsFooterCollectionReusableView", for: indexPath)
            return footerView
        } else
            if (kind == UICollectionView.elementKindSectionHeader) {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "OpenJobsHeaderCollectionReusableView", for: indexPath) as? OpenJobsHeaderView
                headerView?.dotsImageView.isUserInteractionEnabled = true
                let interaction = UIContextMenuInteraction (delegate: self)
                headerView?.dotsImageView.addInteraction (interaction)
                headerView?.categoryLabel.text = arrayJobs[indexPath.section].category
                let postedDate = "Posted: \(viewModel.stringWithShortDateFormatter(date: arrayJobs[indexPath.section].postedDate!))"
                headerView?.postedDateLabel.text = postedDate
                headerView?.statusLabel.text = arrayJobs[indexPath.section].status
                let connectedBusinessesCount = arrayJobs[indexPath.section].connectedBusinesses?.count
                headerView?.connectedLabel.text = (connectedBusinessesCount != nil)
                    ? "You have hired \(connectedBusinessesCount ?? 0) business"
                    : "Connecting you with business"
                return headerView!
        }
        fatalError()
    }
}

extension OpenJobsCollectionViewController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(
            identifier: nil,
            previewProvider: nil) { _ in
                let closeAction = UIAction(
                    title: "Close",
                    image: nil) { _ in
                        self.arrayJobs.remove(at: OpenJobsCollectionViewController.menuIdentifier)
                        self.collectionView.deleteSections(NSIndexSet(index: OpenJobsCollectionViewController.menuIdentifier) as IndexSet)
                        OpenJobsCollectionViewController.menuIdentifier = 0
                }
                return UIMenu(title: "", image: nil, children: [closeAction])
        }
    }
}
