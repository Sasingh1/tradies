//
//  JobsHomeViewController.swift
//  Tradies
//
//  Created by Sartaj Singh on 4/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import UIKit

class JobsHomeViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    lazy var openJobsCollectionViewController: OpenJobsCollectionViewController? = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let viewController = storyBoard.instantiateViewController(withIdentifier: "OpenJobsIdentifier") as? OpenJobsCollectionViewController
            else { return nil }
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "hemberger"), for: .normal)
        button.setTitle("hipages", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.tintColor = UIColor.black
        button.sizeToFit()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        add(asChild: openJobsCollectionViewController)
    }  
}

extension JobsHomeViewController {
    func add(asChild childViewController: UIViewController?) {
        guard let childViewController = childViewController else {
            return
        }
        if children.contains(childViewController) {
            childViewController.removeFromParent()
        }
        addChild(childViewController)
        if containerView.subviews.contains(childViewController.view) {
            childViewController.view.removeFromSuperview()
        }
        containerView.addSubview(childViewController.view)
        childViewController.view.frame = containerView.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childViewController.didMove(toParent: self)
    }
}
