//
//  APIManager.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

struct APIManager {
    private static let baseURLString = "https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/jobs.json"
    static let jobsURL = URL(string: baseURLString)!
}
