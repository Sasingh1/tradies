//
//  JobsInformation.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

struct JobsInformation: Codable {
    var category: String?
    var postedDate: String?
    var status: String?
    var connectedBusinesses: [ConnectedBusinesses]?
    var detailsLink: String?
}

