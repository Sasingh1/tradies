//
//  ConnectedBusinesses.swift
//  Tradies
//
//  Created by Sartaj Singh on 1/2/20.
//  Copyright © 2020 Sartaj Singh. All rights reserved.
//

import Foundation

struct ConnectedBusinesses: Codable {
    var businessId: Int?
    var thumbnail: String?
    var isHired: Bool?
}
